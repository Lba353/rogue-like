﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	public bool isPlayerTurn;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 50;
	public AudioClip gameOverSound;
	public GameObject player;
	public GameObject player2;

	private GameObject HUD;
	private GameObject DifficultyImage;
	private GameObject CharacterImage;
	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject levelImage;
	private GameObject menuImage;
	private Text levelText;
	private Text menuText;
	private bool settingUpGame;
	private int secondsUntilLevelStart = 2;
	private int currentLevel = 1;
	private Button playButton;
	private Button difficultyButton;
	private Button characterButton;

	void Awake() {
		if(Instance != null && Instance != this) {
			DestroyImmediate(gameObject);
			return;
		}

		Instance = this;
		DontDestroyOnLoad (gameObject);
		boardController = GetComponent<BoardController>();
		enemies = new List<Enemy>();
	}

	void Start() {
		menuImage = GameObject.Find("Menu Image");
		menuText = GameObject.Find("Menu Text").GetComponent<Text>();
		menuText.text = "Rogue-Like Project";
		menuImage.SetActive(true);
	}

//Difficulty Sections

	//Regular Game Settings

	public void InitializeGame() {
		Instantiate(player, new Vector3(1f, 1f, 0f), Quaternion.identity);
		HUD = GameObject.Find ("Menu Image");
		CharacterImage = GameObject.Find ("Character Image");
		DifficultyImage = GameObject.Find ("Difficulty Image");
		HUD.SetActive (false);
		CharacterImage.SetActive (false);
		DifficultyImage.SetActive (false);
		settingUpGame = true;
		levelImage = GameObject.Find("Level Image");
		levelText = GameObject.Find("Level Text").GetComponent<Text>();
		levelText.text = "Day " + currentLevel;
		levelImage.SetActive(true);
		enemies.Clear();
		boardController.SetupLevel(currentLevel);
		Invoke ("DisableLevelImage", secondsUntilLevelStart);
	}
	
	private void OnLevelWasLoaded(int levelLoaded) {
		currentLevel++;
		InitializeGame();
	}

	//Easy Game Settings

	public void EasyInitializeGame() {
		Instantiate(player, new Vector3(1f, 1f, 0f), Quaternion.identity);
		DifficultyImage = GameObject.Find ("Difficulty Image");
		DifficultyImage.SetActive (false);
		settingUpGame = true;
		levelImage = GameObject.Find("Level Image");
		levelText = GameObject.Find("Level Text").GetComponent<Text>();
		levelText.text = "Day " + currentLevel;
		levelImage.SetActive(true);
		enemies.Clear();
		boardController.EasySetupLevel(currentLevel);
		Invoke ("DisableLevelImage", secondsUntilLevelStart);
	}

	private void OnEasyLevelWasLoaded(int levelLoaded) {
		currentLevel++;
		EasyInitializeGame();
	}

	//Normal Game Settings

	public void NormalInitializeGame() {
//		if (!DifficultyImage) {
//			CharacterImage = GameObject.Find ("Character Image");
//			CharacterImage.SetActive (false);
//		}
//		else if (!CharacterImage) {
//			DifficultyImage = GameObject.Find ("Difficulty Image");
//			DifficultyImage.SetActive (false);
//		}
		Instantiate(player, new Vector3(1f, 1f, 0f), Quaternion.identity);
		settingUpGame = true;
		levelImage = GameObject.Find("Level Image");
		levelText = GameObject.Find("Level Text").GetComponent<Text>();
		levelText.text = "Day " + currentLevel;
		levelImage.SetActive(true);
		enemies.Clear();
		boardController.NormalSetupLevel(currentLevel);
		Invoke ("DisableLevelImage", secondsUntilLevelStart);
	}

	private void OnNormalLevelWasLoaded(int levelLoaded) {
		currentLevel++;
		NormalInitializeGame();
	}

	//Hard Game Settings

	public void HardInitializeGame() {
		Instantiate(player, new Vector3(1f, 1f, 0f), Quaternion.identity);
		DifficultyImage = GameObject.Find ("Difficulty Image");
		DifficultyImage.SetActive (false);
		settingUpGame = true;
		levelImage = GameObject.Find("Level Image");
		levelText = GameObject.Find("Level Text").GetComponent<Text>();
		levelText.text = "Day " + currentLevel;
		levelImage.SetActive(true);
		enemies.Clear();
		boardController.HardSetupLevel(currentLevel);
		Invoke ("DisableLevelImage", secondsUntilLevelStart);
	}

	private void OnHardLevelWasLoaded(int levelLoaded) {
		currentLevel++;
		HardInitializeGame();
	}

//End of Difficulty Sections

	private void DisableLevelImage() {
		levelImage.SetActive(false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
	}

	void Update() {
		if (isPlayerTurn || areEnemiesMoving || settingUpGame) {
			return;
		}
		StartCoroutine(MoveEnemies ());
	}

	private IEnumerator MoveEnemies() {
		areEnemiesMoving = true;

		yield return new WaitForSeconds (0.2f);

		foreach(Enemy enemy in enemies) {
			enemy.MoveEnemy();
			yield return new WaitForSeconds(enemy.moveTime);
		}

		areEnemiesMoving = false;
		isPlayerTurn = true;
	}

	public void AddEnemyToList(Enemy enemy) {
		enemies.Add(enemy);
	}

	public void GameOver() {
		isPlayerTurn = false;
		SoundController.Instance.music.Stop();
		SoundController.Instance.PlaySingle(gameOverSound);
		levelText.text = "You starved after " + currentLevel + " days...";
		levelImage.SetActive (true);
		enabled = false;
	}

//Menu Switches

	public void CharacterChange() {
		HUD = GameObject.Find ("Menu Image");
		DifficultyImage = GameObject.Find ("Difficulty Image");
		CharacterImage = GameObject.Find("Character Image");
		HUD.SetActive (false);
		DifficultyImage.SetActive (false);
		CharacterImage.SetActive (true);
	}

	public void DifficultyChange() {
		HUD = GameObject.Find ("Menu Image");
		DifficultyImage = GameObject.Find ("Difficulty Image");
		CharacterImage = GameObject.Find("Character Image");
		HUD.SetActive (false);
		DifficultyImage.SetActive (true);
		CharacterImage.SetActive (false);
	}

//End of Menu Switches

//Player Choosing 
	
	//Original Player
	
//	  public void Player() {
//		Instantiate(player, new Vector3(1f, 1f, 0f), Quaternion.identity);
//		NormalInitializeGame();
//	} 

	
	//Lumberjack Player
	
//	 public void Lumber() {
//		Instantiate(player2, new Vector3(1f, 1f, 0f), Quaternion.identity);
//		NormalInitializeGame();
//	} 
	
//End of Player Choosing
}
